# Higher-order programming and type inference

* Anonymous functions
* Function composition
* Currying and partial application
* Folds
